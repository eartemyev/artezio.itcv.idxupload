﻿using System;
using Artezio.Itcv.IdxUpload.Services.Main;

namespace Artezio.Itcv.IdxUpload.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = new Service();
            service.Run("");

        }
    }
}