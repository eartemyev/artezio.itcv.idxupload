﻿namespace Artezio.Itcv.IdxUpload.Domain
{
    public class Xz
    {
        // [Key]
        public long Id { get; set; }

        public An An { get; set; }
        public long AnId { get; set; }

        public string V { get; set; }
        public string X { get; set; }
        public long Y { get; set; }
        public string Z { get; set; }
    }
}