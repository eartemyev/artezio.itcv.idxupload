﻿using System.Collections.Generic;

namespace Artezio.Itcv.IdxUpload.Domain
{
    public class An
    {
        // [Key]
        public long Id { get; set; }

        public string A { get; set; }  // 01
        public string B { get; set; }  // 02
        public string C { get; set; }  // 03
        public string D { get; set; }  // 04
        public string E { get; set; }  // 05
        public string F { get; set; }  // 06
        public string G { get; set; }  // 07
        public string H { get; set; }  // 08
        public string I { get; set; }  // 09
        public string J { get; set; }  // 10
        public string K { get; set; }  // 11
        public string L { get; set; }  // 12
        public string M { get; set; }  // 13
        public string N { get; set; }  // 14

        public ICollection<Xz> Xzs { get; set; }
    }
}