﻿using System.IO;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Artezio.Itcv.IdxUpload.Services.Main
{
    public interface IService
    {
        void RunImport(string directoryRootPath);
        void RunExport(string directoryPath);
    }
    public class Service: IService
    {
        private readonly IdxParser.IService  _parser = new IdxParser.Service();

        public void RunImport(string directoryRootPath)
        {
            using(var dbContext = new Data.AppDbContext())
            {
                var files = new DirectoryInfo(directoryRootPath).GetFiles("*.idx", SearchOption.AllDirectories);
                foreach (var file in files)
                {
                    var entity = _parser.ParseFromFile(file);
                    dbContext.Mdk.Add(entity);

                    dbContext.SaveChanges();
                }
            }
        }

        public void RunExport(string directoryPath)
        {
            var directoryInfo = new DirectoryInfo(directoryPath);
            if (directoryInfo.Exists) directoryInfo.Delete(true);
            directoryInfo.Create();

            using (var dbContext = new Data.AppDbContext())
            {
                PrepareExport(dbContext);

                var anList = dbContext.An
                    .Include(e => e.Xzs)
                    .ToList();

                var i = 1;
                foreach (var an in anList)
                {
                    var filePath = Path.Combine(directoryPath, $"{i}.idx");
                    var text = _parser.Serialize(an);
                    File.WriteAllText(filePath, text);

                    foreach (var anXz in an.Xzs)
                    {
                        File.Copy(anXz.V, Path.Combine(directoryPath, anXz.Z));
                    }

                    i++;
                }
            }
        }

        private void PrepareExport(Data.AppDbContext dbContext)
        {
            #region Query
            var query = @"
set nocount on;

delete from dbo.Xz;
delete from dbo.AN;

insert into dbo.AN(
      A
	, B
	, C
	, D
	, E
	, F
	, G
	, H
	, I
	, J
	, K
	, L
	, M
	, N
)
select distinct 
      A
	, B
	, C
	, D
	, E
	, F
	, G
	, H
	, I
	, J
	, K
	, L
	, M
	, N
from dbo.Mdk;


insert into dbo.Xz(
      Id
	, AnId
	, V
	, X
	, Y
	, Z
)

select distinct 
      mdk.Id
	, an.Id
	, mdk.V
	, mdk.X
	, cast(mdk.Y as bigint)
	, mdk.X + '.' + reverse(SUBSTRING(
		REVERSE(mdk.V)
		, 0
		, PATINDEX('%.%', REVERSE(mdk.V))
	)) V
from dbo.Mdk mdk
inner join dbo.AN an on
        mdk.A = an.A
	and mdk.B = an.B
	and mdk.C = an.C
	and mdk.D = an.D
	and mdk.E = an.E
	and mdk.F = an.F
	and mdk.G = an.G
	and mdk.H = an.H
	and mdk.I = an.I
	and mdk.J = an.J
	and mdk.K = an.K
	and mdk.L = an.L
	and mdk.M = an.M
	and mdk.N = an.N
where 
	    mdk.V is not null
	and mdk.X is not null
	and mdk.Y is not null
;

select *
from dbo.Mdk
where id = 0;
";
            #endregion
            var list = dbContext.Mdk.FromSql(query).ToList();
        }
    }
}