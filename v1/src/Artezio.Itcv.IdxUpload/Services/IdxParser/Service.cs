﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Artezio.Itcv.IdxUpload.Services.IdxParser
{
    // https://bitbucket.org/eartemyev/artezio.itcv.idxupload/issues/1/idx-file-parser
    public interface IService
    {
        Domain.Mdk ParseFromFile(FileInfo file);
        string Serialize(Domain.An entity);
    }
    public class Service: IService
    {
        private const string FieldSeparator = "\r\n";
        public Domain.Mdk ParseFromText(string input)
        {
            var parts = input.Split(new[] { FieldSeparator }, StringSplitOptions.None);
            var builder = new StringBuilder();

            var index = 18;
            while (index < 100)
            {
                if (builder.Length > 0) builder.Append("|");
                builder.Append(parts[index]);

                if (parts[index].Contains("</img>")) break;
                index++;
            }

            var result  = new Domain.Mdk
            {
                A = parts[0],
                B = parts[1],
                C = parts[2],
                D = parts[3],
                E = parts[4],
                F = parts[5],
                G = parts[6],
                H = parts[7],
                I = parts[8],
                J = parts[9],
                K = parts[10],
                L = parts[11],
                M = parts[12],
                N = parts[13],
                O = parts[14],
                P = parts[15],
                Q = parts[16],
                R = parts[17],

                S = builder.ToString(),
                T = parts[index + 1],
                U = null,
                V = null
            };

            return result;
        }

        private FileInfo GetRelatedFile(FileInfo file)
        {
            var relatedFile = file.Directory
                .GetFiles("*.*", SearchOption.TopDirectoryOnly)
                .FirstOrDefault(e => e.Extension != ".idx" && e.Extension != ".thu");
            return relatedFile;
        }

        public Domain.Mdk ParseFromFile(FileInfo file)
        {
            var text = File.ReadAllText(file.FullName);
            var entity = ParseFromText(text);
            entity.U = file.FullName;
            entity.V = GetRelatedFile(file)?.FullName;
            entity.X = Path.GetFileNameWithoutExtension(file.Name);
            entity.Y = long.Parse(entity.X, System.Globalization.NumberStyles.HexNumber).ToString();
            return entity;
        }

        public string Serialize(Domain.An entity)
        {
            var builder = new StringBuilder();

            builder.Append(entity.A);
            builder.Append(FieldSeparator);

            builder.Append(entity.B);
            builder.Append(FieldSeparator);

            builder.Append(entity.C);
            builder.Append(FieldSeparator);

            builder.Append(entity.D);
            builder.Append(FieldSeparator);

            builder.Append(entity.E);
            builder.Append(FieldSeparator);

            builder.Append(entity.F);
            builder.Append(FieldSeparator);

            builder.Append(entity.H);
            builder.Append(FieldSeparator);

            builder.Append(entity.I);
            builder.Append(FieldSeparator);

            builder.Append(entity.J);
            builder.Append(FieldSeparator);

            builder.Append(entity.K);
            builder.Append(FieldSeparator);

            builder.Append(entity.L);
            builder.Append(FieldSeparator);

            builder.Append(entity.M);
            builder.Append(FieldSeparator);

            builder.Append(entity.N);
            builder.Append(FieldSeparator);

            builder.Append("</idx>");
            builder.Append(FieldSeparator);

            builder.Append(Serialize(entity.Xzs));

            builder.Append("</Versichertenakte>");

            return builder.ToString();
        }

        private string Serialize(IEnumerable<Domain.Xz> xz)
        {
            var builder = new StringBuilder();

            builder.Append("<img>");
            builder.Append(FieldSeparator);

            foreach (var item in xz.OrderBy(e => e.Y).ToList())
            {
                builder.Append(item.Z);
                builder.Append(FieldSeparator);
            }

            builder.Append("</img>");
            builder.Append(FieldSeparator);

            return builder.ToString();
        }
    }

}