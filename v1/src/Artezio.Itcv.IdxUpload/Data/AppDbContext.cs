﻿using Microsoft.EntityFrameworkCore;

namespace Artezio.Itcv.IdxUpload.Data
{
    public class AppDbContext : DbContext
    {
        public DbSet<Domain.Mdk> Mdk { get; set; }
        public DbSet<Domain.An> An { get; set; }
        public DbSet<Domain.Xz> Xz { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=MdkDb;Integrated Security=True;");
        }
    }
}