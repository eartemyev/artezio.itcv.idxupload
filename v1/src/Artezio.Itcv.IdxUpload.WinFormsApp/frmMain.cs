﻿using System;
using System.Windows.Forms;
using S = Artezio.Itcv.IdxUpload.Services.Main;

namespace Artezio.Itcv.IdxUpload.WinFormsApp
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtImportPath.Text = System.IO.Directory.GetCurrentDirectory();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            btnImport.Enabled = false;
            Refresh();

            var service =  (S.IService) new S.Service();
            var path  = txtImportPath.Text;
            service.RunImport(path);

            btnImport.Enabled = true;

            MessageBox.Show("Import completed.");
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            btnExport.Enabled = false;
            Refresh();

            var service = (S.IService) new S.Service();
            var path = txtExportPath.Text;
            service.RunExport(path);

            btnExport.Enabled = true;

            MessageBox.Show("Export completed.");
        }
    }
}
