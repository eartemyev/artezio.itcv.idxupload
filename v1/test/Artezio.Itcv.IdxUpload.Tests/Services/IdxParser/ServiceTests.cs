using System.Collections.Generic;
using Artezio.Itcv.IdxUpload.Domain;
using Xunit;
using S = Artezio.Itcv.IdxUpload.Services;

namespace Artezio.Itcv.IdxUpload.Tests.Services.IdxParser
{
    public class ServiceTests
    {
        // https://bitbucket.org/eartemyev/artezio.itcv.idxupload/issues/1
        [Fact]
        public void ParseFromText()
        {
            var text = Helpers.EmbeddedResource.Helper.LoadString(typeof(ServiceTests), "./397A0B0.idx");
            Assert.True(text.StartsWith("<Versichertenakte>"));
            Assert.True(text.EndsWith("</Versichertenakte>"));

            var service = new S.IdxParser.Service();
            var result = service.ParseFromText(text);

            Assert.Equal("<Versichertenakte>", result.A);
            Assert.Equal("0", result.B);
            Assert.Equal("<idx>", result.C);
            Assert.Equal("138164870", result.D);
            Assert.Equal("pfl", result.E);
            Assert.Equal("", result.F);
            Assert.Equal("31.01.2017", result.G);
            Assert.Equal("BKK Schwenninger Villingen-Schwenningen", result.H);
            Assert.Equal("Itzehoe", result.I);
            Assert.Equal("Speckien", result.J);
            Assert.Equal("Renate", result.K);
            Assert.Equal("16.06.1938", result.L);
            Assert.Equal("Pflege.Vers.", result.M);
            Assert.Equal("Gutachten", result.N);
            Assert.Equal("48A2AE6985C69F37C1257BB000326585", result.O);
            Assert.Equal("", result.P);
            Assert.Equal("", result.Q);
            Assert.Equal("</idx>", result.R);
            Assert.Equal("<img>|ga_138164870_0000n.PDF|reha_138164870_0001n.PDF|pfa_138164870_0002n.PDF|himi_138164870_0003n.PDF|</img>", result.S);
            Assert.Equal("</Versichertenakte>", result.T);
            Assert.Equal(null, result.U);
            Assert.Equal(null, result.V);
        }
        // https://bitbucket.org/eartemyev/artezio.itcv.idxupload/issues/3
        [Fact]
        public void Serialize()
        {
            var expected = Helpers.EmbeddedResource.Helper.LoadString(typeof(ServiceTests), "./Serialize.idx");
            Assert.True(expected.StartsWith("A"));
            Assert.True(expected.EndsWith("</Versichertenakte>"));

            var entity = new Domain.An
            {
                A = "A",
                B = "B",
                C = "C",
                D = "D",
                E = "E",
                F = "F",
                G = "G",
                H = "H",
                I = "I",
                J = "J",
                K = "K",
                L = "L",
                M = "M",
                N = "N",
                Xzs = new List<Xz>
                {
                    new Xz { Y = 3, Z = "C3.jpg"},
                    new Xz { Y = 1, Z = "A1.PDF"},
                    new Xz { Y = 2, Z = "B2.txt"},
                }
            };

            var service = new S.IdxParser.Service();
            var result = service.Serialize(entity);

            Assert.Equal(expected, result);
        }
    }
}
