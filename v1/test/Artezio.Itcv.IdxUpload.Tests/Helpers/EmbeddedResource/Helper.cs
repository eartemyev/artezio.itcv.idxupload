﻿using System;
using System.IO;
using System.Reflection;

namespace Artezio.Itcv.IdxUpload.Tests.Helpers.EmbeddedResource
{
    public class Helper
    {
        public static string LoadString(Type type, string path)
        {
            var assembly = type.GetTypeInfo().Assembly;
            if (path.StartsWith("./") || path.StartsWith(@".\\"))
            {
                path =$"{type.Namespace}.{path.Substring(2)}";
            }
            path = path.Replace(@"\\", ".").Replace("/", ".");
            using (var resourceStream = assembly.GetManifestResourceStream(path))
            {
                if (resourceStream == null)
                {
                    throw new Exception($"Embedded resource '{path}' was not found.");
                }

                using (var reader = new StreamReader(resourceStream))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }
}
