﻿CREATE TABLE [dbo].[AN]
(
	[Id] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [A] NVARCHAR(2048) NULL,
    [B] NVARCHAR(2048) NULL,
    [C] NVARCHAR(2048) NULL,
    [D] NVARCHAR(2048) NULL,
    [E] NVARCHAR(2048) NULL,
    [F] NVARCHAR(2048) NULL,
    [G] NVARCHAR(2048) NULL,
    [H] NVARCHAR(2048) NULL,
    [I] NVARCHAR(2048) NULL,
    [J] NVARCHAR(2048) NULL,
    [K] NVARCHAR(2048) NULL,
    [L] NVARCHAR(2048) NULL,
    [M] NVARCHAR(2048) NULL,
    [N] NVARCHAR(2048) NULL,
)
